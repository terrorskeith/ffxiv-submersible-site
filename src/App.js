import SubmersibleParts from "./components/submersible-parts/SubmersibleParts";
import SubmersibleStats from "./components/submersible-stats/SubmersibleStats";
import RequiredStats from "./components/required-stats/RequiredStats";
import PossibleCombinations from "./components/possible-combinations/PossibleCombinations";

function App() {
  return (
    <div className="App" style={{display: "flex", flexDirection: "row"}}>
      <SubmersibleStats />
      <SubmersibleParts />
      <RequiredStats />
      <PossibleCombinations />
    </div>
  );
}

export default App;
