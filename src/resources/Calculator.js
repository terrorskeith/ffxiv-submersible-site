const { SubmersibleParts } = require("./SubmersibleParts");

class Calculator {
    constructor(minSurveillance, minRetrieval, minSpeed, minRange, minFavor) {
        const submersibleParts = new SubmersibleParts().submersibleParts;

        submersibleParts.hulls.forEach(hull => {
            submersibleParts.sterns.forEach(stern => {
                submersibleParts.bridges.forEach(bridge => {
                    submersibleParts.bows.forEach(bow => {
                        console.log(hull.name + ", " + stern.name + ", " + bridge.name + ", " + bow.name)
                    });
                });
            });
        });
    }

}

new Calculator(0,0,0,0,0)
