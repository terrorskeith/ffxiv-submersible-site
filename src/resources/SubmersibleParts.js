// import {readFileSync, promises as fsPromises} from 'fs';

const {readFileSync} = require('fs');


class SubmersibleParts {

    constructor(){
        const contents = readFileSync("./SubmersibleParts.txt", 'utf-8');

        this.submersibleParts = JSON.parse(contents);
    }
}

module.exports.SubmersibleParts = SubmersibleParts;
