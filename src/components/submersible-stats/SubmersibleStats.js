import {Component} from "react";

class SubmersibleStats extends Component {
    render () {
        return (
            <div style={{display:"flex", flexFlow:"column"}}>
                <div style={{display:"flex", flexFlow:"row"}}>
                    <p>Base Surveillance</p>
                    <input name="Base Surveillance" />
                </div>
                <div style={{display:"flex", flexFlow:"row"}}>
                    <p>Base Retrieval</p>
                    <input name="Base Retrieval" />
                </div>
                <div style={{display:"flex", flexFlow:"row"}}>
                    <p>Base Speed</p>
                    <input name="Base Speed" />
                </div>
                <div style={{display:"flex", flexFlow:"row"}}>
                    <p>Base Range</p>
                    <input name="Base Range" />
                </div>
                <div style={{display:"flex", flexFlow:"row"}}>
                    <p>Base Favor</p>
                    <input name="Base Favor" />
                </div>
            </div>
        )
    }
}

export default SubmersibleStats;