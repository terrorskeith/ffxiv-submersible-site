import {Component, useState} from "react";

class RequiredStats extends Component {

    constructor(props) {
        super(props);

        this.state = {
            level: "23",
            surveillance: "43",
            retrieval: "43",
            speed: "34",
            range: "$3",
            favor: "43"
        }
    }

    handleSubmit = (e)=> {
        if(e.target.name === "levelInput") {
            this.setState({level: e.target.value})
        } else if(e.target.name === "surveillanceInput") {
            this.setState({surveillance: e.target.value})
        } else if(e.target.name === "retrievalInput") {
            this.setState({retrieval: e.target.value})
        } else if(e.target.name === "speedInput") {
            this.setState({speed: e.target.value})
        } else if(e.target.name === "rangeInput") {
            this.setState({range: e.target.value})
        } else if(e.target.name === "favorInput") {
            this.setState({favor: e.target.value})
        }
    }

    /*handleSubmit = (e)=> {
        // Prevent the browser from reloading the page
        e.preventDefault();

        // Read the form data
        const form = e.target;
        const formData = new FormData(form);

        // You can pass formData as a fetch body directly:
        fetch('/some-api', { method: form.method, body: formData });

        // Or you can work with it as a plain object:
        const formJson = Object.fromEntries(formData.entries());
        console.log(formJson);
    }*/

    render () {
        return (
            <div>
                Level: <input name="levelInput" defaultValue="Some initial value" onChange={this.handleSubmit} />
                Surveillance: <input name="surveillanceInput" defaultValue="Some initial value" onChange={this.handleSubmit} />
                Retrieval: <input name="retrievalInput" defaultValue="Some initial value" onChange={this.handleSubmit} />
                Speed: <input name="speedInput" defaultValue="Some initial value" onChange={this.handleSubmit} />
                Range: <input name="rangeInput" defaultValue="Some initial value" onChange={this.handleSubmit} />
                Favor: <input name="favorInput" defaultValue="Some initial value" onChange={this.handleSubmit} />
                <p>{this.state.level}</p>
                <p>{this.state.surveillance}</p>
                <p>{this.state.retrieval}</p>
                <p>{this.state.speed}</p>
                <p>{this.state.range}</p>
                <p>{this.state.favor}</p>
            </div>
        )
    }
}

export default RequiredStats;